Rails.application.routes.draw do
  # change root as static_pages's home action
  root 'static_pages#home'
  get 'static_pages/help'
  get 'static_pages/about'
end

